#!/bin/sh
#set -e
# USER_ID=1000, USER_LOGIN=www-data, HOME_DIR=/var/www

# needed for xdebug purposes
echo export PHP_IDE_CONFIG="serverName=$APP_DOMAIN" >> $HOME_DIR/.env
echo export PHP_IDE_CONFIG="serverName=$APP_DOMAIN" >> $HOME_DIR/.bashrc
export APP_DIR=/opt/app

# exporing env vars to use them in CRON processes
printenv | sed 's/^\([^=]*\)=\(.*\)$/export \1=\"\2\"/g' >> $HOME_DIR/.env

make build

chown "$USER_LOGIN":"$USER_LOGIN" /opt/app -R
chmod 0777 -R "$APP_DIR"/var

exec "$@"
